# Desafio: Calculadora JS

Esta é uma atividade da matéria **Linguagem e padrões web** do curso de pós graduação em **Tecnologias para Aplicações Web** da Unopar, cursada a partir de Setembro / 2022.

Desafio: a tarefa consiste em fazer uma calculadora com apenas **HTML, CSS e Javascript**.