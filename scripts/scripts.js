document.addEventListener("DOMContentLoaded", () => {
    let current = "0";
    let stored = "0";
    const visor = document.querySelector(".visor");
    visor.innerText = current;
    document.querySelectorAll(".btn").forEach((btn) => {
        const btnVal = btn.innerText;
        ["click", "touchstart"].forEach((evt) => {
            btn.addEventListener(evt, () => {
                if (btn.classList.contains("btn-primary")) {
                    if (current === "0") {
                        current = btnVal;
                    } else {
                        current += btnVal;
                    }
                    visor.innerText = current;
                }
                if (btnVal == ".") {
                    if (!current.includes(".")) {
                        current += ".";
                        visor.innerText = current;
                    }
                }
                console.log(btnVal);
            });
        });
    });
});
